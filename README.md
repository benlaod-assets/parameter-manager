## Requirements

[Game Variables](https://gitlab.com/benlaod-assets/game-variables)

## How to install it ?

In your package manager, click on the '+' button then 'Add package by name...'.
Then past the following URL `fr.benjaminbrasseur.parametermanager`.

## How to use it ?

Past intro `Parameters Path` the path where you store your Game Variables. (ex: `Assets/GameParameters/Variables`)
Then, click on "Refresh path".

If you put your variables inside folders, they will be segmented based on the directory where they are stored.

Edit your values, then click on "Save" to save the updated variables.
If you want to cancel, click on "Reset".
