﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using BenlaodAssets.GameVariables.Runtime.Scripts.ScriptableObjects;
using UnityEditor;
using UnityEngine;

namespace BenlaodAssets.ParameterManager.Editor.Scripts.Herlpers.ParametersEditor
{
    public class ParametersEditor : EditorWindow
    {
        [MenuItem("Helpers/Parameters Editor")]
        private static void ShowWindow()
        {
            var window = GetWindow<ParametersEditor>();
            window.titleContent = new GUIContent("Parameters");
            window.Show();
        }

        private Dictionary<string, float> _floatList = new();
        private Dictionary<string, int> _intList = new();
        private Dictionary<string, string> _stringList = new();
        private Dictionary<string, Color> _colorList = new();

        private string _parametersPath = "Assets/GameParameters";

        private string[] _folders;

        private void OnGUI()
        {
            _parametersPath = EditorGUILayout.TextField("Parameters Path", _parametersPath);
            if (GUILayout.Button("Refresh path"))
            {
                _floatList = new Dictionary<string, float>();
                _intList = new Dictionary<string, int>();
                _stringList = new Dictionary<string, string>();
                _colorList = new Dictionary<string, Color>();

                _folders = Directory.GetDirectories(_parametersPath);
                Repaint();
            }

            if (_folders == null) return;

            foreach (var folder in _folders)
            {
                EditorGUILayout.LabelField(folder.Split('\\').Last(), EditorStyles.boldLabel);
                foreach (var findAsset in AssetDatabase.FindAssets("t:ScriptableObject", new[] { folder }))
                {
                    var path = AssetDatabase.GUIDToAssetPath(findAsset);
                    var asset = AssetDatabase.LoadMainAssetAtPath(path);
                    if (AssetDatabase.GetMainAssetTypeAtPath(path) == typeof(FloatVariable))
                    {
                        if (!_floatList.ContainsKey(path))
                        {
                            _floatList[path] = ((FloatVariable)asset).value;
                        }

                        _floatList[path] = EditorGUILayout.FloatField(asset.name, _floatList[path]);
                    }
                    else if (AssetDatabase.GetMainAssetTypeAtPath(path) == typeof(IntVariable))
                    {
                        if (!_intList.ContainsKey(path))
                        {
                            _intList[path] = ((IntVariable)asset).value;
                        }

                        _intList[path] = EditorGUILayout.IntField(asset.name, _intList[path]);
                    }
                    else if (AssetDatabase.GetMainAssetTypeAtPath(path) == typeof(StringVariable))
                    {
                        if (!_stringList.ContainsKey(path))
                        {
                            _stringList[path] = ((StringVariable)asset).value;
                        }

                        _stringList[path] = EditorGUILayout.TextField(asset.name, _stringList[path]);
                    }
                    else if (AssetDatabase.GetMainAssetTypeAtPath(path) == typeof(ColorVariable))
                    {
                        if (!_stringList.ContainsKey(path))
                        {
                            _colorList[path] = ((ColorVariable)asset).value;
                        }

                        _colorList[path] = EditorGUILayout.ColorField(asset.name, _colorList[path]);
                    }
                }
            }

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Save"))
            {
                SetDirty(_floatList);
                SetDirty(_intList);
                SetDirty(_stringList);
                SetDirty(_colorList);

                AssetDatabase.SaveAssets();
            }

            if (GUILayout.Button("Reset"))
            {
                _floatList = new Dictionary<string, float>();
                _intList = new Dictionary<string, int>();
                _stringList = new Dictionary<string, string>();
                Repaint();
            }

            GUILayout.EndHorizontal();
        }

        private static void SetDirty<T>(Dictionary<string, T> list)
        {
            foreach (var (key, value) in list)
            {
                var asset = (Variable<T>)AssetDatabase.LoadMainAssetAtPath(key);
                EditorUtility.SetDirty(asset);
                asset.value = value;
            }
        }
    }
}